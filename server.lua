
local chemlog = "esx_illegal:pickedUpChemicals";

ESX = nil


-- Laddar hem ESX objektet
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent(chemlog)
AddEventHandler(chemlog, function()

  local source = source
  local player = ESX.GetPlayerFromId(source)
  local msg = ('Player %s picked up 1 chemicals.'):format(player.identifier)
  local time = "[" .. os.date('%Y-%m-%d %H:%M:%S') .."]"
  local position = ("" .. player.getCoords(true)):gsub("vector3","")

  print("[ALBINS:LOG]" .. time .. position .. msg)
end)
