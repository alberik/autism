dependencies {
    'NativeUI',
}

-- Klient scripts. Skickas till alla spelares fivem klienter.  
client_script {
	'client.lua',
}

 -- Server scripten. Kör på servern.
server_script {
	'server.lua',
}
