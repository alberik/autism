# Autism #
Autism heter det här pluginet. Det är ett plugin som ska logga händelser i spelet FiveM. Det har varit ett stort problem på servern Paradise City, därav skapas denna plugin skapa utförliga loggar som håller koll på servern. .

### Vad som loggas ###
Exempel på loggade rader:

	[2020-04-22 20:48:29](817.7, -3195.3, 5.9) Player steam:113010517d085c3 picked up 1 chemicals.
	[2020-04-22 20:48:42](814.4, -3189.6, 5.9) Player steam:113010517d085c3 picked up 1 chemicals.
	[2020-04-22 20:48:53](811.6, -3187.5, 5.9) Player steam:113010517d085c3 picked up 1 chemicals.
	[2020-04-22 20:49:07](810.2, -3188.5, 5.9) Player steam:113010517d085c3 picked up 1 chemicals.
	[2020-04-22 20:49:20](810.1, -3188.7, 5.9) Player steam:113010517d085c3 picked up 1 chemicals.

### Todo ###
Det finns mycket saker som behöver förändras på programmet. Mer generella saker som återstår att lösa på pluginet är:
* Bilmekanikern
	* Börja logga `esx_lscustom` samt `esx_mechanicjob` för att fälla tjuvar. Logga vem som uppgraderar bilar och fakturor som mekaniker betalar.

* Droger
	* Skapa en mer generaliserad procedur för att logga drogplock.
	* Skapa ett program som i efterhand analyserar filerna för att analysera flödet av droger till pengar. Se om man kan hitta fuskare som dupear eller liknande.

### Installation ###
* Klona repon till resources mappen.
```bash
 git clone https://alberik@bitbucket.org/alberik/autism.git "server/resources/[albin]/autism"
```

* Starta pluginet i din serverkonfiguration
(ofta `server.cfg` )
>
```    
start esx_...
start esx_...
start esx_...
start autism
```

### Who do I talk to? ###
Bidra gärna till projektet genom att kontakta mig för att få tillgång till git repositoryn:
Kontakt:
>Kontakt: 	Albin (aka alberik)  
>Discord:   Albin#2446              
>Epost      [<alberik@protonmail.com>](mailto:alberik@protonmail.com)
